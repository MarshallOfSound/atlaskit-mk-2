# @atlaskit/atlassian-switcher

## 2.0.1
- Updated dependencies [ed41cac6ac](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ed41cac6ac):
  - @atlaskit/item@10.0.1
  - @atlaskit/theme@9.0.3
  - @atlaskit/lozenge@9.0.0

## 2.0.0
- Updated dependencies [4b07b57640](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4b07b57640):
  - @atlaskit/button@13.0.2
  - @atlaskit/icon@17.0.2
  - @atlaskit/navigation@35.1.1
  - @atlaskit/logo@12.0.0

## 1.1.1
- [patch] [d216253463](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d216253463):

  - Fix trigger xflow invoked with incorrect arguments

## 1.1.0
- [minor] [b3381f5c07](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b3381f5c07):

  - Add domain to analytics

## 1.0.0
- [minor] [7c17b35107](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7c17b35107):

  - Updates react and react-dom peer dependencies to react@^16.8.0 and react-dom@^16.8.0. To use this package, please ensure you use at least this version of react and react-dom.
- Updated dependencies [7c17b35107](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7c17b35107):
  - @atlaskit/analytics-gas-types@4.0.4
  - @atlaskit/docs@8.0.0
  - @atlaskit/analytics-next@5.0.0
  - @atlaskit/button@13.0.0
  - @atlaskit/drawer@4.0.0
  - @atlaskit/icon@17.0.0
  - @atlaskit/item@10.0.0
  - @atlaskit/logo@11.0.0
  - @atlaskit/lozenge@8.0.0
  - @atlaskit/navigation@35.0.0
  - @atlaskit/theme@9.0.0
  - @atlaskit/type-helpers@4.0.0
  - @atlaskit/analytics-namespaced-context@4.0.0

## 0.5.0
- [minor] [59024ff4c5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/59024ff4c5):

  - Always include Jira Service Desk cross sell link if the instance does not have Jira Service Desk license.

## 0.4.8
- [patch] [7f9fd0ddfc](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7f9fd0ddfc):

  - Improve error messages and analytics

## 0.4.7
- [patch] [3eeb2ccf51](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3eeb2ccf51):

  - Update translations

## 0.4.6
- [patch] [0a4ccaafae](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0a4ccaafae):

  - Bump tslib

## 0.4.5
- Updated dependencies [9c0b4744be](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9c0b4744be):
  - @atlaskit/docs@7.0.3
  - @atlaskit/button@12.0.3
  - @atlaskit/drawer@3.0.7
  - @atlaskit/icon@16.0.9
  - @atlaskit/item@9.0.1
  - @atlaskit/logo@10.0.4
  - @atlaskit/lozenge@7.0.2
  - @atlaskit/navigation@34.0.4
  - @atlaskit/theme@8.1.7

## 0.4.4
- [patch] [3f28e6443c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3f28e6443c):

  - @atlaskit/analytics-next-types is deprecated. Now you can use types for @atlaskit/analytics-next supplied from itself.

## 0.4.3
- Updated dependencies [1e826b2966](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e826b2966):
  - @atlaskit/docs@7.0.2
  - @atlaskit/analytics-next@4.0.3
  - @atlaskit/drawer@3.0.6
  - @atlaskit/icon@16.0.8
  - @atlaskit/logo@10.0.3
  - @atlaskit/navigation@34.0.3
  - @atlaskit/theme@8.1.6
  - @atlaskit/button@12.0.0

## 0.4.2
- [patch] [d13fad66df](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d13fad66df):

  - Enable esModuleInterop for typescript, this allows correct use of default exports

## 0.4.1
- [patch] [bcb3d443fc](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bcb3d443fc):

  - Addressing QA fixes

## 0.4.0
- [minor] [e36f791fd6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e36f791fd6):

  - Improve types

## 0.3.6
- [patch] [db2a7ffde6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/db2a7ffde6):

  - Fixing recent containers bug

## 0.3.5
- [patch] [9d6f8d516a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9d6f8d516a):

  - Adding expand link support to Atlassian Switcher

## 0.3.4
- [patch] [571ad59bb7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/571ad59bb7):

  - Pacakge version and feature flag analytics

## 0.3.3
- [patch] [9cf7af0d03](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9cf7af0d03):

  - Data provider analytics

## 0.3.2
- [patch] [aacc698f07](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/aacc698f07):

  - Adds an analytics event to track atlassian switcher dissmisals using the triggerXFlow callback

## 0.3.1
- [patch] [57f774683f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/57f774683f):

  - Move @atlaskit/logo to peer dependencies

## 0.3.0
- [minor] [68443e3d6f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/68443e3d6f):

  - Opsgenie app switching support

## 0.2.3
- [patch] [a041506c4d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a041506c4d):

  - Fixes a bug in global-navigation caused due to a missing asset in atlassian-switcher

## 0.2.2
- [patch] [1bcaa1b991](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1bcaa1b991):

  - Add npmignore for index.ts to prevent some jest tests from resolving that instead of index.js

## 0.2.1
- [patch] [94acafec27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/94acafec27):

  - Adds the error page according to the designs.

## 0.2.0
- [minor] [9d5cc39394](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9d5cc39394):

  - Dropped ES5 distributables from the typescript packages

## 0.1.4
- [patch] [b08df363b7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b08df363b7):

  - Add atlassian-switcher prefetch trigger in global-navigation

## 0.1.3
- [patch] [269cd93118](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/269cd93118):

  - Progressive loading and prefetch primitives

## 0.1.2
- [patch] [6ca66fceac](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6ca66fceac):

  - Add enableSplitJira to allow multiple jira link displayed if there are jira products

## 0.1.1
- Updated dependencies [76299208e6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/76299208e6):
  - @atlaskit/button@10.1.3
  - @atlaskit/icon@16.0.4
  - @atlaskit/analytics-gas-types@3.2.5
  - @atlaskit/analytics-namespaced-context@2.2.1
  - @atlaskit/docs@7.0.0
  - @atlaskit/analytics-next@4.0.0
  - @atlaskit/drawer@3.0.0
  - @atlaskit/item@9.0.0
  - @atlaskit/logo@10.0.0
  - @atlaskit/lozenge@7.0.0
  - @atlaskit/navigation@34.0.0
  - @atlaskit/theme@8.0.0

## 0.1.0
- [minor] [6ee7b60c4a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6ee7b60c4a):

  - Create generic switcher for satellite products

## 0.0.9
- [patch] [e7fa9e1308](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7fa9e1308):

  - Fixing icon imports

## 0.0.8
- [patch] [ebfdf1e915](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ebfdf1e915):

  - Update sections and grouping according to updated designs

## 0.0.7
- [patch] [8a70a0db9f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8a70a0db9f):

  - SSR compatibility fix

## 0.0.6
- [patch] [3437ac9990](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3437ac9990):

  - Firing events according to minimum event spec

## 0.0.5
- [patch] [9184dbf08b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9184dbf08b):

  - Fixing package.json issue with atlassian-switcher

## 0.0.4
- [patch] [95d9a94bd0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/95d9a94bd0):

  - Adding root index for atlassian-switcher

## 0.0.3
- [patch] [b56ca0131d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b56ca0131d):

  - Attempting to fix flow issue where atlassian-switcher is not recognized

## 0.0.2
- [patch] [235f937d66](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/235f937d66):

  - Initial package release

## 0.0.1
- [patch] [25921b9e50](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/25921b9e50):

  - Adding AtlassianSwitcher component
