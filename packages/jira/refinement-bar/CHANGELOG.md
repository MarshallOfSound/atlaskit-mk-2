# @atlaskit/refinement-bar

## 0.4.2
- Updated dependencies [6dd86f5b07](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6dd86f5b07):
  - @atlaskit/avatar@16.0.2
  - @atlaskit/icon@17.1.1
  - @atlaskit/theme@9.0.2
  - @atlaskit/section-message@4.0.0

## 0.4.1
- [patch] [da4644083b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da4644083b):

  - Remove files key in package.json

## 0.4.0
- [minor] [3dab4a0906](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3dab4a0906):

  - add license and npmignore

## 0.3.0
- [minor] [00a1cd96c3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/00a1cd96c3):

  - use verbose export syntax and remove root index.js

## 0.2.0
- [minor] [7c17b35107](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7c17b35107):

  - Updates react and react-dom peer dependencies to react@^16.8.0 and react-dom@^16.8.0. To use this package, please ensure you use at least this version of react and react-dom.

## 0.1.6
- Updated dependencies [d3cad2622e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d3cad2622e):
  - @atlaskit/docs@7.0.4
  - @atlaskit/button@12.0.5

## 0.1.5
- Updated dependencies [2020ab9db1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2020ab9db1):
  - @atlaskit/section-message@2.0.4

## 0.1.4
- Updated dependencies [6cdf11238d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6cdf11238d):
  - @atlaskit/textfield@1.0.0

## 0.1.3
- Updated dependencies [9c0b4744be](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9c0b4744be):
  - @atlaskit/docs@7.0.3
  - @atlaskit/avatar@15.0.4
  - @atlaskit/badge@11.0.1
  - @atlaskit/button@12.0.3
  - @atlaskit/icon@16.0.9
  - @atlaskit/icon-object@3.0.8
  - @atlaskit/section-message@2.0.3
  - @atlaskit/select@8.1.1
  - @atlaskit/spinner@10.0.7
  - @atlaskit/textfield@0.4.4
  - @atlaskit/tooltip@13.0.4
  - @atlaskit/theme@8.1.7

## 0.1.2
- Updated dependencies [1e826b2966](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e826b2966):
  - @atlaskit/docs@7.0.2
  - @atlaskit/analytics-next@4.0.3
  - @atlaskit/avatar@15.0.3
  - @atlaskit/icon@16.0.8
  - @atlaskit/icon-object@3.0.7
  - @atlaskit/section-message@2.0.2
  - @atlaskit/select@8.0.5
  - @atlaskit/spinner@10.0.5
  - @atlaskit/textfield@0.4.3
  - @atlaskit/theme@8.1.6
  - @atlaskit/tooltip@13.0.3
  - @atlaskit/button@12.0.0

## 0.1.1
- [patch] [153c6bd4ce](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/153c6bd4ce):

  - expose public exports

## 0.1.0
- [minor] [998ea1170e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/998ea1170e):

  - Introduce alpha version of the refinement bar package.
