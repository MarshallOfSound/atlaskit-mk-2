# @atlaskit/updater-cli

## 2.0.3
- [patch] [0a4ccaafae](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0a4ccaafae):

  - Bump tslib
- [patch] [0ac39bd2dd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0ac39bd2dd):

  - Bump tslib to 1.9

## 2.0.2
- [patch] [d13fad66df](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d13fad66df):

  - Enable esModuleInterop for typescript, this allows correct use of default exports

## 2.0.1
- [patch] [25544ecbc8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/25544ecbc8):

  - Ignore tsconfig from being published

## 2.0.0
- [major] [9d5cc39394](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9d5cc39394):

  - Dropped ES5 distributables from the typescript packages

## 1.0.1
- [patch] [dd30b8f831](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/dd30b8f831):

  - Add missing dependencies

## 1.0.0
- [major] [bc47f570b2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bc47f570b2):

  - MVP of editor updater cli
