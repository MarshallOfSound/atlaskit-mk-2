export const articleExamples = [
  {
    id: '00',
    title: 'Article 00',
    body: `<div style="height:0;flex:1 0 auto;position:relative;border-style:none"><object type="text/html" style="display:block;position:absolute;top:0;left:0;height:100%;width:100%;opacity:0;overflow:hidden;pointer-events:none;z-index:-1" aria-hidden="true" tabindex="-1"></object></div><div class="sc-lkqHmb ljojFd"><div class="ak-renderer-document"><p>Bacon ipsum dolor amet brisket porchetta andouille spare ribs pig sausage. Frankfurter pork loin cow, flank swine tongue short loin fatback beef ribs ball tip sausage alcatra salami.</p><ul><li><p>Brisket</p></li><li><p>Venison</p></li><li><p>Leberkas</p></li></ul><p>Jowl drumstick ham hock boudin tongue biltong sausage t-bone meatloaf pork belly kielbasa tri-tip. Ball tip shankle tenderloin strip steak, pork hamburger flank cow. Filet mignon sirloin beef ribs bresaola swine bacon pork chop sausage t-bone drumstick pork loin landjaeger shank capicola. Shankle bacon corned beef ball tip flank filet mignon rump pig buffalo pork loin fatback beef tongue.</p><p>Rump buffalo leberkas chicken meatball ball tip alcatra capicola ham hock. Beef jerky kevin filet mignon drumstick. Pork chop rump meatball swine, porchetta salami drumstick short loin prosciutto spare ribs tongue boudin kevin. Pork loin jowl cow sirloin, buffalo filet mignon pig sausage salami capicola. Leberkas brisket cow burgdoggen shank.</p><p>Buffalo meatball hamburger jowl t-bone frankfurter meatloaf jerky pork biltong tongue chuck sirloin. Drumstick ground round ribeye prosciutto buffalo, filet mignon turkey shoulder swine corned beef pig tri-tip pastrami landjaeger. Hamburger filet mignon fatback shank t-bone ham. Brisket landjaeger fatback ground round ham short ribs shank flank bacon burgdoggen turducken. Doner boudin tenderloin meatloaf leberkas, t-bone flank. Jerky porchetta pancetta boudin buffalo landjaeger swine ball tip ground round drumstick frankfurter tail shankle venison kielbasa.</p><p><span><div class="text-fragment" style="display:inline-block"><div style="height:0;flex:1 0 auto;position:relative;border-style:none"><object type="text/html" style="display:block;position:absolute;top:0;left:0;height:100%;width:100%;opacity:0;overflow:hidden;pointer-events:none;z-index:-1" aria-hidden="true" tabindex="-1"></object></div><div class="sc-lkqHmb ljojFd"><div class="ak-renderer-document"><p>Click <span><span><span><span data-testid="icon" style="white-space:normal"><span class="Icon__IconWrapper-dyhwwi-0 bcqBjl"><svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M13 11V3.993A.997.997 0 0 0 12 3c-.556 0-1 .445-1 .993V11H3.993A.997.997 0 0 0 3 12c0 .557.445 1 .993 1H11v7.007c0 .548.448.993 1 .993.556 0 1-.445 1-.993V13h7.007A.997.997 0 0 0 21 12c0-.556-.445-1-.993-1H13z" fill="currentColor" fill-rule="evenodd"/></svg></span></span></span></span></span>&gt; <strong>Invite a user</strong> in the sidebar. <span><span><span><span data-testid="icon" style="white-space:normal"><span class="Icon__IconWrapper-dyhwwi-0 bcqBjl"><svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><g fill="currentColor"><path d="M10 5a1 1 0 0 0 .997 1h3.006a1 1 0 0 0 0-2h-3.006C10.45 4 10 4.45 10 5zM8 5c0-1.657 1.347-3 2.997-3h3.006a3 3 0 0 1 0 6h-3.006A3 3 0 0 1 8 5zm2 7a1 1 0 0 0 .997 1h3.006a1 1 0 0 0 0-2h-3.006c-.547 0-.997.45-.997 1zm-2 0c0-1.657 1.347-3 2.997-3h3.006a3 3 0 0 1 0 6h-3.006A3 3 0 0 1 8 12zm2 7a1 1 0 0 0 .997 1h3.006a1 1 0 0 0 0-2h-3.006c-.547 0-.997.45-.997 1zm-2 0c0-1.657 1.347-3 2.997-3h3.006a3 3 0 0 1 0 6h-3.006A3 3 0 0 1 8 19z"/><path d="M16.062 6h2.415c.812 0 1.472.66 1.472 1.47v1.954c0 .815-.654 1.47-1.475 1.47H16v2h2.474a3.466 3.466 0 0 0 3.475-3.47V7.47A3.472 3.472 0 0 0 18.477 4h-2.415v2zm-7.175 5H6.472A3.472 3.472 0 0 0 3 14.47v1.954c0 1.92 1.55 3.47 3.474 3.47H8.95v-2H6.474c-.82 0-1.474-.655-1.474-1.47V14.47c0-.81.66-1.47 1.472-1.47h2.415v-2z"/></g></svg></span></span></span></span></span></p></div></div></div></span></p></div></div>`,
    externalLink: 'http://google.com',
    relatedArticles: [
      {
        id: '01',
        title: 'Article 01',
        description: 'Description of Article 01',
      },
      {
        id: '02',
        title: 'Article 02',
        description: 'Description of Article 02',
      },
      {
        id: '03',
        title: 'Article 03',
        description: 'Description of Article 03',
      },
      {
        id: '04',
        title: 'Article 04',
        description: 'Description of Article 04',
      },
      {
        id: '05',
        title: 'Article 05',
        description: 'Description of Article 05',
      },
      {
        id: '06',
        title: 'Article 06',
        description: 'Description of Article 06',
      },
      {
        id: '07',
        title: 'Article 07',
        description: 'Description of Article 07',
      },
    ],
  },
  {
    id: '01',
    title: 'Article 01',
    body: `<div style="height:0;flex:1 0 auto;position:relative;border-style:none"><object type="text/html" style="display:block;position:absolute;top:0;left:0;height:100%;width:100%;opacity:0;overflow:hidden;pointer-events:none;z-index:-1" aria-hidden="true" tabindex="-1"></object></div><div class="sc-lkqHmb ljojFd"><div class="ak-renderer-document"><p>Bacon ipsum dolor amet brisket porchetta andouille spare ribs pig sausage. Frankfurter pork loin cow, flank swine tongue short loin fatback beef ribs ball tip sausage alcatra salami.</p><p> </p></div></div>`,
    externalLink: 'http://google.com',
    relatedArticles: [
      {
        id: '02',
        title: 'Article 02',
        description: 'Description of Article 02',
      },
      {
        id: '03',
        title: 'Article 03',
        description: 'Description of Article 03',
      },
      {
        id: '04',
        title: 'Article 04',
        description: 'Description of Article 04',
      },
      {
        id: '05',
        title: 'Article 05',
        description: 'Description of Article 05',
      },
      {
        id: '06',
        title: 'Article 06',
        description: 'Description of Article 06',
      },
      {
        id: '07',
        title: 'Article 07',
        description: 'Description of Article 07',
      },
    ],
  },
  {
    id: '02',
    title: 'Article 02',
    body: `<div style="height:0;flex:1 0 auto;position:relative;border-style:none"><object type="text/html" style="display:block;position:absolute;top:0;left:0;height:100%;width:100%;opacity:0;overflow:hidden;pointer-events:none;z-index:-1" aria-hidden="true" tabindex="-1"></object></div><div class="sc-lkqHmb ljojFd"><div class="ak-renderer-document"><p>Bacon ipsum dolor amet brisket porchetta andouille spare ribs pig sausage. Frankfurter pork loin cow, flank swine tongue short loin fatback beef ribs ball tip sausage alcatra salami.</p><p> </p></div></div>`,
    externalLink: 'http://google.com',
    relatedArticles: [
      {
        id: '01',
        title: 'Article 01',
        description: 'Description of Article 01',
      },
      {
        id: '03',
        title: 'Article 03',
        description: 'Description of Article 03',
      },
      {
        id: '04',
        title: 'Article 04',
        description: 'Description of Article 04',
      },
      {
        id: '05',
        title: 'Article 05',
        description: 'Description of Article 05',
      },
      {
        id: '06',
        title: 'Article 06',
        description: 'Description of Article 06',
      },
      {
        id: '07',
        title: 'Article 07',
        description: 'Description of Article 07',
      },
    ],
  },
  {
    id: '03',
    title: 'Article 03',
    body: `<div style="height:0;flex:1 0 auto;position:relative;border-style:none"><object type="text/html" style="display:block;position:absolute;top:0;left:0;height:100%;width:100%;opacity:0;overflow:hidden;pointer-events:none;z-index:-1" aria-hidden="true" tabindex="-1"></object></div><div class="sc-lkqHmb ljojFd"><div class="ak-renderer-document"><p>Bacon ipsum dolor amet brisket porchetta andouille spare ribs pig sausage. Frankfurter pork loin cow, flank swine tongue short loin fatback beef ribs ball tip sausage alcatra salami.</p><p> </p></div></div>`,
    externalLink: 'http://google.com',
    relatedArticles: [
      {
        id: '01',
        title: 'Article 01',
        description: 'Description of Article 01',
      },
      {
        id: '02',
        title: 'Article 02',
        description: 'Description of Article 02',
      },
      {
        id: '04',
        title: 'Article 04',
        description: 'Description of Article 04',
      },
      {
        id: '05',
        title: 'Article 05',
        description: 'Description of Article 05',
      },
      {
        id: '06',
        title: 'Article 06',
        description: 'Description of Article 06',
      },
      {
        id: '07',
        title: 'Article 07',
        description: 'Description of Article 07',
      },
    ],
  },
  {
    id: '04',
    title: 'Article 04',
    body: `<div style="height:0;flex:1 0 auto;position:relative;border-style:none"><object type="text/html" style="display:block;position:absolute;top:0;left:0;height:100%;width:100%;opacity:0;overflow:hidden;pointer-events:none;z-index:-1" aria-hidden="true" tabindex="-1"></object></div><div class="sc-lkqHmb ljojFd"><div class="ak-renderer-document"><p>Bacon ipsum dolor amet brisket porchetta andouille spare ribs pig sausage. Frankfurter pork loin cow, flank swine tongue short loin fatback beef ribs ball tip sausage alcatra salami.</p><p> </p></div></div>`,
    externalLink: 'http://google.com',
    relatedArticles: [
      {
        id: '01',
        title: 'Article 01',
        description: 'Description of Article 01',
      },
      {
        id: '02',
        title: 'Article 02',
        description: 'Description of Article 02',
      },
      {
        id: '03',
        title: 'Article 03',
        description: 'Description of Article 03',
      },
      {
        id: '05',
        title: 'Article 05',
        description: 'Description of Article 05',
      },
      {
        id: '06',
        title: 'Article 06',
        description: 'Description of Article 06',
      },
      {
        id: '07',
        title: 'Article 07',
        description: 'Description of Article 07',
      },
    ],
  },
  {
    id: '05',
    title: 'Article 05',
    body: `<div style="height:0;flex:1 0 auto;position:relative;border-style:none"><object type="text/html" style="display:block;position:absolute;top:0;left:0;height:100%;width:100%;opacity:0;overflow:hidden;pointer-events:none;z-index:-1" aria-hidden="true" tabindex="-1"></object></div><div class="sc-lkqHmb ljojFd"><div class="ak-renderer-document"><p>Bacon ipsum dolor amet brisket porchetta andouille spare ribs pig sausage. Frankfurter pork loin cow, flank swine tongue short loin fatback beef ribs ball tip sausage alcatra salami.</p><p> </p></div></div>`,
    externalLink: 'http://google.com',
    relatedArticles: [
      {
        id: '01',
        title: 'Article 01',
        description: 'Description of Article 01',
      },
      {
        id: '02',
        title: 'Article 02',
        description: 'Description of Article 02',
      },
      {
        id: '03',
        title: 'Article 03',
        description: 'Description of Article 03',
      },
      {
        id: '04',
        title: 'Article 04',
        description: 'Description of Article 04',
      },
      {
        id: '06',
        title: 'Article 06',
        description: 'Description of Article 06',
      },
      {
        id: '07',
        title: 'Article 07',
        description: 'Description of Article 07',
      },
    ],
  },
  {
    id: '06',
    title: 'Article 06',
    body: `<div style="height:0;flex:1 0 auto;position:relative;border-style:none"><object type="text/html" style="display:block;position:absolute;top:0;left:0;height:100%;width:100%;opacity:0;overflow:hidden;pointer-events:none;z-index:-1" aria-hidden="true" tabindex="-1"></object></div><div class="sc-lkqHmb ljojFd"><div class="ak-renderer-document"><p>Bacon ipsum dolor amet brisket porchetta andouille spare ribs pig sausage. Frankfurter pork loin cow, flank swine tongue short loin fatback beef ribs ball tip sausage alcatra salami.</p><p> </p></div></div>`,
    externalLink: 'http://google.com',
    relatedArticles: [
      {
        id: '01',
        title: 'Article 01',
        description: 'Description of Article 01',
      },
      {
        id: '02',
        title: 'Article 02',
        description: 'Description of Article 02',
      },
      {
        id: '03',
        title: 'Article 03',
        description: 'Description of Article 03',
      },
      {
        id: '04',
        title: 'Article 04',
        description: 'Description of Article 04',
      },
      {
        id: '05',
        title: 'Article 05',
        description: 'Description of Article 05',
      },
      {
        id: '07',
        title: 'Article 07',
        description: 'Description of Article 07',
      },
    ],
  },
  {
    id: '07',
    title: 'Article 07',
    body: `<div style="height:0;flex:1 0 auto;position:relative;border-style:none"><object type="text/html" style="display:block;position:absolute;top:0;left:0;height:100%;width:100%;opacity:0;overflow:hidden;pointer-events:none;z-index:-1" aria-hidden="true" tabindex="-1"></object></div><div class="sc-lkqHmb ljojFd"><div class="ak-renderer-document"><p>Bacon ipsum dolor amet brisket porchetta andouille spare ribs pig sausage. Frankfurter pork loin cow, flank swine tongue short loin fatback beef ribs ball tip sausage alcatra salami.</p><p> </p></div></div>`,
    externalLink: 'http://google.com',
    relatedArticles: [
      {
        id: '01',
        title: 'Article 01',
        description: 'Description of Article 01',
      },
      {
        id: '02',
        title: 'Article 02',
        description: 'Description of Article 02',
      },
      {
        id: '03',
        title: 'Article 03',
        description: 'Description of Article 03',
      },
      {
        id: '04',
        title: 'Article 04',
        description: 'Description of Article 04',
      },
      {
        id: '05',
        title: 'Article 05',
        description: 'Description of Article 05',
      },
      {
        id: '06',
        title: 'Article 06',
        description: 'Description of Article 06',
      },
    ],
  },
];

export const articlesSearchExample = [
  {
    id: '01',
    title: 'Article 01',
    description: 'Description of Article 01',
  },
  {
    id: '02',
    title: 'Article 02',
    description: 'Description of Article 02',
  },
  {
    id: '03',
    title: 'Article 03',
    description: 'Description of Article 03',
  },
  {
    id: '04',
    title: 'Article 04',
    description: 'Description of Article 04',
  },
  {
    id: '05',
    title: 'Article 05',
    description: 'Description of Article 05',
  },
  {
    id: '06',
    title: 'Article 06',
    description: 'Description of Article 06',
  },
  {
    id: '07',
    title: 'Article 07',
    description: 'Description of Article 07',
  },
];

export const getArticle = (id = '00') => {
  const articleIndex = parseInt(id, 10);
  return new Promise(resolve => resolve(articleExamples[articleIndex]));
};

export const searchArticle = (value: string) => {
  if (value === 'empty') {
    return new Promise(resolve => resolve([]));
  } else {
    return new Promise(resolve => resolve(articlesSearchExample));
  }
};
