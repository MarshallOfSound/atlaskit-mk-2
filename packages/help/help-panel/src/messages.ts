import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  help_panel_header: {
    id: 'global_help.header',
    defaultMessage: 'Help',
    description: '',
  },
  help_panel_navigation_back: {
    id: 'global_help.navigation.back',
    defaultMessage: 'Back',
    description: '',
  },
  help_panel_search_placeholder: {
    id: 'global_help.search.placeholder',
    defaultMessage: 'Search for help',
    description: '',
  },
  help_panel_article_rating_title: {
    id: 'global_help.article_rating.title',
    defaultMessage: 'Was this helpful?',
    description: '',
  },
  help_panel_article_rating_option_yes: {
    id: 'global_help.article_rating.option.yes',
    defaultMessage: 'Yes',
    description: '',
  },
  help_panel_article_rating_option_no: {
    id: 'global_help.article_rating.option.no',
    defaultMessage: 'No',
    description: '',
  },
  help_panel_article_rating_form_title: {
    id: 'global_help.article_rating.form.title',
    defaultMessage: 'Tell us why you gave this rating?',
    description: '',
  },
  help_panel_article_rating_accurate: {
    id: 'global_help.article_rating.accurate',
    defaultMessage: `It isn't accurate`,
    description: '',
  },
  help_panel_article_rating_clear: {
    id: 'global_help.article_rating.clear',
    defaultMessage: `It isn't clear`,
    description: '',
  },
  help_panel_article_rating_relevant: {
    id: 'global_help.article_rating.relevant',
    defaultMessage: `It isn't relevant`,
    description: '',
  },
  help_panel_related_article_title: {
    id: 'global_help.related_article.title',
    defaultMessage: 'RELATED',
    description: '',
  },
  help_panel_related_article_show_more: {
    id: 'global_help.related_article.show_more',
    defaultMessage: 'Show more related',
    description: '',
  },
  help_panel_related_article_show_less: {
    id: 'global_help.related_article.show_less',
    defaultMessage: 'Show less related',
    description: '',
  },
  help_panel_search_results_title: {
    id: 'global_help.search_results.title',
    defaultMessage: 'Search results',
    description: '',
  },
  help_panel_search_results_suggestions: {
    id: 'global_help.search_results.suggestions',
    defaultMessage: 'SUGGESTIONS',
    description: '',
  },
  help_panel_search_results_no_results: {
    id: 'global_help.search_results.no_results',
    defaultMessage: `No matching search results`,
    description: '',
  },
});
