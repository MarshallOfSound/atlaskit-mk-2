## 0.1.0

## 0.5.5
- Updated dependencies [6dd86f5b07](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6dd86f5b07):
  - @atlaskit/form@6.0.2
  - @atlaskit/icon@17.1.1
  - @atlaskit/navigation@35.1.2
  - @atlaskit/theme@9.0.2
  - @atlaskit/help-article@0.4.4
  - @atlaskit/section-message@4.0.0

## 0.5.4
- [patch] [84b7ee2f8b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/84b7ee2f8b):

  - fix articles loading when the articleId changes

## 0.5.3
- [patch] [45f063521d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/45f063521d):

  - Updated dependencies

## 0.5.2
- [patch] [d1854796ae](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d1854796ae):

  - Updated dependencies

## 0.5.1
- [patch] [ccacfe8570](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ccacfe8570):

  - Updated help-article version

## 0.5.0
- [minor] [88b9f3568b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/88b9f3568b):

  - Update transition configuration of the panel. If the initial value of isOpen is true, fire analytics event and request the article

## 0.4.2
- [patch] [4053dcd740](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4053dcd740):

  - added close button and styles for header when the component renders the default content

## 0.4.1
- [patch] [a77b18b718](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a77b18b718):

  - fix - Display DefaultContent

## 0.4.0
- [minor] [f479974eb4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f479974eb4):

  - Added version.json and update analytics.json to read the info from there

## 0.3.0
- [minor] [875ff270e8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/875ff270e8):

  - Use @atlaskit/help-article instead of custom component

## 0.2.0
- [minor] [e6b180d4cd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e6b180d4cd):

  - First release of global-help
- [major] First release of global-help
