# @atlaskit/help-article

## 0.4.4
- Updated dependencies [6dd86f5b07](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6dd86f5b07):
  - @atlaskit/theme@9.0.2
  - @atlaskit/section-message@4.0.0

## 0.4.3
- [patch] [75efe3ab05](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/75efe3ab05):

  - Updated dependencies

## 0.4.2
- [patch] [36558f8fb2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/36558f8fb2):

  - Updated CSS styles

## 0.4.1
- [patch] [7ad5551b05](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7ad5551b05):

  - Updated/fix CSS styles

## 0.4.0
- [minor] [05460c129b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/05460c129b):

  - Added prop titleLinkUrl to make the title of the article a link

## 0.3.1
- [patch] [d3a2a15890](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d3a2a15890):

  - Made HelpArticle the default export (fix)

## 0.3.0
- [minor] [801e8de151](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/801e8de151):

  - Made HelpArticle the default export. Added styles from Contentful

## 0.2.0
- [minor] [d880cc2200](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d880cc2200):

  - First release of global-article
